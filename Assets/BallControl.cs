﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    // Rigidbody 2D bola
    private Rigidbody2D rigidBody2D;

    // Besarnya gaya awal yang diberikan untuk mendorong bola
    public float xInitialForce;
    public float yInitialForce;

    [SerializeField]
    private float speed;

    // Titik asal lintasan bola saat ini
    private Vector2 trajectoryOrigin;

    void ResetBall()
    {
        //Reset posisi menjadi 0,0
        transform.position = Vector2.zero;

        //Reset kecepatan menjadi 0
        rigidBody2D.velocity = Vector2.zero;
    }

    void PushBall()
    {
        float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);

        float randomDirection = Random.Range(1, 5);


        switch(randomDirection)
        {
            case 1:
                rigidBody2D.AddForce(new Vector2(xInitialForce, yInitialForce));
                break;
            case 2:
                rigidBody2D.AddForce(new Vector2(xInitialForce, -yInitialForce));
                break;
            case 3:
                rigidBody2D.AddForce(new Vector2(-xInitialForce, yInitialForce));
                break;
            case 4:
                rigidBody2D.AddForce(new Vector2(-xInitialForce, -yInitialForce));
                break;

        }
        //if(randomDirection < 1.0f)
        //{
        //    rigidBody2D.AddForce(new Vector2(-xInitialForce, yInitialForce));
        //}

        //else
        //{
        //    rigidBody2D.AddForce(new Vector2(xInitialForce, yInitialForce));
        //}
        //
    }

    void RestartGame()
    {
        ResetBall();

        Invoke("PushBall", 2);
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();

        RestartGame();

        trajectoryOrigin = transform.position;
    }


    // Source: https://www.awesomeinc.org/tutorials/unity-pong/
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            Vector2 vel;
            vel.x = rigidBody2D.velocity.x;
            vel.y = (rigidBody2D.velocity.y / 2) + (collision.collider.attachedRigidbody.velocity.y / 3);
            rigidBody2D.velocity = vel;
        }
    }

    private void FixedUpdate()
    {
        if(rigidBody2D.velocity.magnitude != speed)
        {
            rigidBody2D.velocity = rigidBody2D.velocity.normalized * speed;
        }
    }

    // Ketika bola beranjak dari sebuah tumbukan, rekam titik tumbukan tersebut
    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }

    // Untuk mengakses informasi titik asal lintasan
    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin; }
    }

}
